<?php
require('Solver.php');

class SolverHelper extends Solver
{
    private $equation;

    private $valuesArray;
    private $operatorsArray;

    public function  __construct(string $equation)
    {
        $this->equation = $equation;
        $this->equationParser();
        if (!$this->equationParser()) {
            throw new Exception('Wrong equation!');
        }

        parent::__construct($this->divisibleConstructor());
    }

    private function equationParser ():bool
    {
        $matches = [];

        if($this->validation()){
            preg_match("/^(([1-9][0-9]?)?x\^[4]*[+-])/", $this->equation, $matches);
            $this->valuesArray[0] = intval($matches[0]) == 0 ? 1 : intval($matches[0]);
            $this->operatorsArray[0] = substr($matches[0], -1);

            preg_match("/(([1-9][0-9]?)?x\^[3]*[+-])/", $this->equation, $matches);
            $this->valuesArray[1] = intval($matches[0]) == 0 ? 1 : intval($matches[0]);
            $this->operatorsArray[1] = substr($matches[0], -1);

            preg_match("/(([1-9][0-9]?)?x\^[2]*[+-])/", $this->equation, $matches);
            $this->valuesArray[2] = intval($matches[0]) == 0 ? 1 : intval($matches[0]);
            $this->operatorsArray[2] = substr($matches[0], -1);

            preg_match("/(([1-9][0-9]?)?x[+-])/", $this->equation, $matches);
            $this->valuesArray[3] = intval($matches[0]) == 0 ? 1 : intval($matches[0]);
            $this->operatorsArray[3] = substr($matches[0], -1);

            preg_match("/([1-9][0-9]?)$/", $this->equation, $matches);
            $this->valuesArray[4] = intval($matches[0]);

            return true;
        }

        return false;
    }

    private function validation():bool
    {
        if(preg_match("/^(([1-9][0-9]?)?x\^[4]*[+-])+(([1-9][0-9]?)?x\^[3]*[+-])+(([1-9][0-9]?)?x\^[2]*[+-])+(([1-9][0-9]?)?x[+-])+([1-9][0-9]?)$/", $this->equation)){
            return true;
        }
        return false;
    }

    private function divisibleConstructor():array
    {
        $divisibleArray = [];
        $oddsArray = array_reverse($this->valuesArray);
        $operatorsArray = array_reverse($this->operatorsArray);
        foreach ($oddsArray as $k=>$odd){
            if($operatorsArray[$k] == '+' || $k == 4){
                $divisibleArray[$k] = $odd;
            }
            else{
                $divisibleArray[$k] = $odd*(-1);
            }
        }

        return array_reverse($divisibleArray);
    }
}