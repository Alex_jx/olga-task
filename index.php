<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Olga Pivovar</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<form class="decor">
    <div class="form-left-decoration"></div>
    <div class="form-right-decoration"></div>
    <div class="circle"></div>
    <div class="form-inner">
        <h3>Write the equation</h3>
        <input type="text" name="equation" id="equation" placeholder="ax^4+bx^3+cx^2+dx+e">
        <input type="submit" id="submit" value="Submit">
    </div>
</form>
<div class="decor answer" style="display: none;">
    <div class="form-left-decoration"></div>
    <div class="form-right-decoration"></div>
    <div class="circle"></div>
    <div class="form-inner">
        <span>Decision</span>
    </div>
</div>
<script
    src="https://code.jquery.com/jquery-3.5.0.min.js"
    integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
    crossorigin="anonymous"></script>
<script src="js/main.js"></script>
</body>
</html>