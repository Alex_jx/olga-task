<?php


class Printer
{
    public static $html;

    public static function divider(int $divider):string
    {
        $html = '<div>
                    <span>Подходит делитель: '.$divider.'</span>
                </div>';

        return $html;
    }

    public static function freeMember(array $dividers, int $member):string
    {
        $block = '';
        foreach ($dividers as $divider){
            $block .= $divider.', ';
        }
        $block = substr($block,0,-2);

        $html = '<div>
                    <span>Свободный член уравнения: '.$member.'</span>
                    <span>Его делители: '.$block.'</span>
                </div>';

        return $html;
    }

    public static function division(array $divider, array $divisibles, int $exponent):string
    {
        $block = '';
        foreach ($divisibles as $k=>$divisible){
            if($k > 0){
                if($divisible > 0){
                    if($exponent === 1){
                        $block .= '+'.$divisible.'x';
                    }
                    else if($exponent === 0){
                        $block .= '+'.$divisible;
                    }
                    else{
                        $block .= '+'.$divisible.'x^'.$exponent;
                    }
                }
                else{
                    if($exponent === 1){
                        $block .= $divisible.'x';
                    }
                    else if($exponent === 0)
                    {
                        $block .= $divisible;
                    }
                    else{
                        $block .= $divisible.'x^'.$exponent;
                    }
                }
            }
            else{
                $block .= $divisible.'x^'.$exponent;
            }
            $exponent--;
        }
        if($block[0].$block[1] === '1x'){
            $block = substr($block, 1);
        }

        if($divider[0] > 0){
            $dividerStr = '+'.$divider[0];
        }
        else{
            $dividerStr = $divider[0];
        }

        $html = '<div>
                    <span>Делим: '.$block.'</span>
                    <span>На: x'.$dividerStr.'</span>
                </div>';

        return $html;
    }

    public static function divisionResult($divisibles, $exponent):string
    {
        $block = '';
        foreach ($divisibles as $k=>$divisible){
            if($k > 0){
                if($divisible > 0){
                    if($exponent === 1){
                        $block .= '+'.$divisible.'x';
                    }
                    else if($exponent === 0){
                        $block .= '+'.$divisible;
                    }
                    else{
                        $block .= '+'.$divisible.'x^'.$exponent;
                    }
                }
                else{
                    if($exponent === 1){
                        $block .= $divisible.'x';
                    }
                    else if($exponent === 0)
                    {
                        $block .= $divisible;
                    }
                    else{
                        $block .= $divisible.'x^'.$exponent;
                    }
                }
            }
            else{
                $block .= $divisible.'x^'.$exponent;
            }
            $exponent--;
        }

        if($block[0].$block[1] === '1x'){
            $block = substr($block, 1);
        }

        $html = '<div>
                    <span>Получим уравнение: '.$block.'</span>
                </div>';

        return $html;
    }

    public static function roots(array $roots)
    {
        $block = '';
        foreach ($roots as $k=>$root)
        {
            $block .= '<span>x'.($k+1).' = '.$root.' </span>';
        }
        $html = '<div>
                    <span>Корни уравнения: '.$block.'</span>
                </div>';

        return $html;
    }
}