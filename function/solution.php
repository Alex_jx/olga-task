<?php
require('../src/SolverHelper.php');
if(isset($_POST['equation'])) {
    try {
        $solve = new SolverHelper($_POST['equation']);
    } catch (Exception $e) {
        echo 'Exception: '. $e->getMessage() ."\n";
        die();
    }
}
echo $solve->solve();
die();

?>

