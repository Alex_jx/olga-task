$( document ).ready(function() {
    $('#submit').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "function/solution.php",
            data: {
                equation: $('#equation').val(),
            },
            success: function( data ){
                    $('.answer').show();
                    $('.answer .form-inner').html(data);
            },
        });
    })
});
