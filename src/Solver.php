<?php
require('Printer.php');
class Solver
{
    private $divisibleArray;
    private $dividerArray;

    private $exponent = 4;

    private $html;


    public function __construct(array $divisibleArray)
    {
        $this->divisibleArray = $divisibleArray;
    }

    public function solve(): string
    {
        $dividers = $this->getDividers(end($this->divisibleArray));
        $this->html .= Printer::freeMember($dividers, end($this->divisibleArray));

        while($this->exponent > 2){
            $iterator = 1;
            foreach ($dividers as $k=>$divider){
                if($this->checkDivider($divider)){
                    $this->html .= Printer::divider($divider);
                    $this->divisibleArray = $this->division($divider);
                    $this->exponent--;
                    $this->html .= Printer::divisionResult($this->divisibleArray, $this->exponent);
                    break;
                }
                $iterator++;
            }
            if($iterator > count($dividers)){
                break;
            }
        }

        if($iterator > count($dividers)){
            $this->html = 'Wrong equation. Not found divider!';
        }
        else{
            $roots = $this->quadraticEquation($this->divisibleArray[0], $this->divisibleArray[1], $this->divisibleArray[2]);
            $this->html .= Printer::roots($roots);
        }


        return $this->html;
    }

    private function getDividers(int $numeric):array
    {
        $dividers = [];
        for ($i = 1; $i<=abs($numeric); $i++){
            if(is_int($numeric/$i)){
                array_push($dividers, $i);
                array_push($dividers, $i*(-1));
            }
        }

        return $dividers;
    }

    private function checkDivider(int $divider):bool
    {
        $result = 0;
        $exponent = $this->exponent;
        foreach ($this->divisibleArray as $divisible){
            if($exponent){
                $result += $divisible * pow($divider, $exponent);
            }
            else{
                $result += $divisible;
            }
            $exponent--;
        }

        if($result == 0){
            return true;
        }

        return false;
    }

    private function division(int $divider): array
    {
        $multipliers = [];
        $this->dividerArray = [
            0 => $divider*(-1),
            1 => 1
        ];
        $this->html .= Printer::division($this->dividerArray, $this->divisibleArray, $this->exponent);
        $this->divisibleArray = array_reverse($this->divisibleArray);


        while (count($multipliers) < $this->exponent){
            $multipliers = $this->divisionStep($multipliers);
        }

        return $multipliers;
    }

    private function divisionStep(array $multipliers): array
    {
        while(count($this->divisibleArray) > count($this->dividerArray))
        {
            array_unshift($this->dividerArray, 0);
        }
        Printer::division($this->divisibleArray , $this->dividerArray, $this->exponent);
        array_push($multipliers, end($this->divisibleArray)/end($this->dividerArray));
        $multiplier = end($this->divisibleArray)/end($this->dividerArray);

        $dividerHelpArray = $this->dividerArray;
        foreach($dividerHelpArray as $k=>$divider){
            $dividerHelpArray[$k] = $divider * $multiplier;
        }

        foreach($this->divisibleArray as $k=>$divisible){
            $this->divisibleArray[$k] = $divisible - $dividerHelpArray[$k];
        }

        $this->divisibleArray = array_reverse($this->divisibleArray);
        foreach($this->divisibleArray as $k=>$divisible){
            if($divisible === 0){
                unset($this->divisibleArray[$k]);
                unset($this->dividerArray[$k]);
            }
            else{
                break;
            }
        }
        $this->divisibleArray = array_reverse($this->divisibleArray);
        $this->dividerArray = array_values($this->dividerArray);
        Printer::division($this->divisibleArray , $this->dividerArray, $this->exponent);

        return $multipliers;
    }

    private function quadraticEquation(int $a, int $b, int $c):array
    {
        if ($b==0) {
            if ($c<0) {
                $x1 = sqrt(abs($c/$a));
                $x2 = sqrt(abs($c/$a));
            } elseif ($c==0) {
                $x1 = $x2 = 0;
            } else {
                $x1 = sqrt($c/$a).'i';
                $x2 = -sqrt($c/$a).'i';
            }
        } else {
            $d = $b*$b-4*$a*$c;
            if ($d>0) {
                $x1 = (-$b+sqrt($d))/2*$a;
                $x2 = (-$b-sqrt($d))/2*$a;
            } elseif ($d==0) {
                $x1 = $x2 = (-$b)/2*$a;
            } else {
                $x1 = -$b . '+' . sqrt(abs($d)) . 'i';
                $x2 = -$b . '-' . sqrt(abs($d)) . 'i';
            }
        }
        return array($x1, $x2);

    }

}